## Budget extraction

These notebooks extracts csv data from pdf files of provincial budgets
located at https://www.bb.go.th/web/budget/province/province_bud66/

The most challenging task is to figuring out the indentation of each
item.  The text positions from the pdf give the right offsets of the
items; thus, the first approach attempted is to calculate text widths,
but that turns out to be error-prone.

The current approach is to capture the pdf as images and perform
indentation search directly from the images.  This is very slow but
surprisingly reliable.

See the following images.

The right offsets are shown in red rectangles.  The indents located are shown in green.
![Locating indents](locating-indents.png)

We compute the separation thresholds based on statistics from Bangkok pdf file.
![Indents and separation thresholds](indents-seps.png)

The codes contain way too many hacks.  The current version does not
properly recover English parts of the items.  We should try a better
pdf correction codes soon.

Source pdfs are from: https://www.bb.go.th/web/budget/province/province_bud66/

The current output csv files in [raw-csv](raw-csv) are raw tables from pdf
files.  It would be nicer (for later processing) to duplicate parent
row items to their child rows.  We will work on that very soon.

**Remarks:** The data extracted surely contain errors.  We provide the extracted data
"as is" with no warranty on the quality of the data.  If you find any errors, please
report; thanks so much!


Thanks to [Rocket Media Lab](https://rocketmedialab.co/) for this nice
project.  We would also like to thank Dollapak Suwanpunya for discussion and 
the idea on using OCR to locate indents, and Chaiporn Jaikaeo for pdf image capturing idea.
